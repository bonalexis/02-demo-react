import { useState } from 'react';
import '../styles/NameGenerator.css'
import PersonCard from './PersonCard';

function NameGenerator() {
    const [persons, setPersons] = useState([]);
    const [display, setDisplay] = useState("card");

    function addNewLine() {
        fetch('https://randomuser.me/api')
            .then(response => response.json())
            .then(data => data.results[0])
            .then(person => setPersons(persons.concat([person])));
    }

    function changeDisplay() {

    }

    function deletePerson(person) {
        setPersons(persons.filter(p => p.login.uuid !== person.login.uuid))
    }

    return (
        <>
            <div className="NameGenerator">
                <div className="container">
                    <h1>Name generator</h1>
                    <button onClick={addNewLine}>Nouvelle personne</button>
                    <button onClick={() => setDisplay(display === "card" ? "csv" : "card")}>Changer l'affichage</button>
                    <div className='persons'>
                        {display === "card" && persons.map(person => <PersonCard key={person.login.uuid} person={person} onDelete={deletePerson} />)}
                        {display === "csv" && (
                            <code lang='csv'>
                                FirstNam; LastName; City;<br />
                                {persons.map(person => {
                                    return <span>{person.name.first}; {person.name.last}; {person.location.city}<br /></span>
                                } )}
                            </code>
                        )}
                    </div>
                </div>
            </div>
        </>
    );
}

export default NameGenerator;
